﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FpuCrawler.Factories;
using FpuCrawler.Models;
using MoreLinq;

namespace FpuCrawler {
	class Crawler {

		public static List<Page> Nodes = new List<Models.Page>();

		public static Uri Root { get; private set; }

		public int NumberOfThreads { get; private set; }

		public void SetUp(Uri root , int numOfThreads) {
			Root = root;
			this.NumberOfThreads = numOfThreads;
			Nodes.Add(PageFactory.CreateNewPage(root));
		}

		public void Crawl() {

			Task.Run(() => {
				CrawlTask(Nodes[0]).Wait();
				ChangeCrawlState(Nodes[0] , CrawlerState.Crawled).Wait();
			}).Wait();

			List<Task> tasks = new List<Task>();
			while(Nodes.Count(n => n.CrawlState == CrawlerState.Crawled && n.CrawlState == CrawlerState.DontCrawl) != Nodes.Count) {
				while(tasks.Count < this.NumberOfThreads + 1) {
					var nodes = GetRelaventNodes();
					foreach(var item in nodes) {
						if(tasks.Count < this.NumberOfThreads + 1) {
							ChangeCrawlState(item , CrawlerState.Crawling).Wait();
							tasks.Add(Task.Run(() => {
								try {
									CrawlTask(item).Wait();
								}
								catch(Exception e) {

									Console.WriteLine(item.PageUri);
									CrawlTask(item).Wait();
								}

								ChangeCrawlState(item , CrawlerState.Crawled).Wait();
							}));
						}
						else { break; }
					}

					System.Threading.Thread.Sleep(100);
				}

				while(tasks.Any(t => t.Status != TaskStatus.RanToCompletion)) {
					System.Threading.Thread.Sleep(100);
				}

				var tIndexes = tasks.Where(t => t.Status == TaskStatus.RanToCompletion).Select(t => tasks.IndexOf(t)).ToArray();

				foreach(var index in tIndexes) {
					tasks.Remove(tasks.ElementAt(index));
					break;
				}

				Nodes = Nodes.DistinctBy(n => n.PageUri).ToList();
			}
		}

		public Page[] GetRelaventNodes() {
			var t1 = Task.Run(() => {
				return Nodes.Where(n => n.CrawlState == CrawlerState.UnCrawled).ToArray();
			});
			return t1.Result;
		}

		private async Task CrawlTask(Page node) {
			var links = node.GetLinks().ToArray();
			var count1 = links.Length / 2;
			var nodes = Nodes;
			Console.WriteLine("Crawling: {0}" , node.PageUri);
			var t1 = Task.Run(() => {
				for(int i = 0 ; i < count1 ; i++) {
					if(!nodes.Any(n => n.PageUri == links[i].AbsoluteUri)) {
						var page = PageFactory.CreateNewPage(links[i]);
						Nodes.Add(page);
						Console.WriteLine("ADDED -> {0}" , page.PageUri);
					}
				}

			});

			var t2 = Task.Run(() => {
				for(int i = count1 ; i < links.Length ; i++) {
					if(!nodes.Any(n => n.PageUri == links[i].AbsoluteUri)) {
						var page = PageFactory.CreateNewPage(links[i]);
						Nodes.Add(page);
						Console.WriteLine("ADDED -> {0}" , page.PageUri);
					}
				}
			});

			await t1;
			await t2;
			Console.WriteLine("Crawled: {0}" , node.PageUri);
		}

		public async Task ChangeCrawlState(Page node , CrawlerState crawlState) {
			await Task.Run(() => {
				node.CrawlState = crawlState;
			});
		}
	}
}
