﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace FpuCrawler {
	class ParseLinks {
		public static IEnumerable<Uri> Parse(string html) {
			var doc = new HtmlDocument();

			try { doc.LoadHtml(html); }
			catch(Exception) { return null; }

			return doc.DocumentNode.Descendants("a").Where(a => a.Attributes["href"] != null)
				.Select(a => a.Attributes["href"].Value)
				.Where(a => !a.Contains("#") && !a.Contains("mailto:") && !a.Contains("tel:") && !string.IsNullOrEmpty(a))
				.Distinct()
				.Select(u => u.Contains("http://") || u.Contains("https://") ? new Uri(u) : new Uri(Crawler.Root.AbsoluteUri + u.Substring(1)));
		}
	}
}
