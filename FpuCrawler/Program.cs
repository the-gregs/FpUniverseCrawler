﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FpuCrawler.Models;
using FpuCrawler.Factories;

namespace FpuCrawler {
	class Program {

		static List<Page> Pages = new List<Page>();

		static void Main(string[] args) {
			var crawler = new Crawler();
			crawler.SetUp(new Uri("https://floridapolytechnic.org/") , 10);

			crawler.Crawl();

			Console.WriteLine("Done...");
		}
	}
}
