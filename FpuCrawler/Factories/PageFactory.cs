﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using FpuCrawler.Models;

namespace FpuCrawler.Factories {
	public class PageFactory {

		public static Page CreateNewPage(Uri uri) {
			HttpResponseMessage responseMessage = null;
			try {
				responseMessage = new UrlContentFetcher().FetchHttpResponseHeader(uri);
			}
			catch(TaskCanceledException e) {
				if(!e.CancellationToken.IsCancellationRequested) {
					System.Threading.Thread.Sleep(1000);
					responseMessage = new UrlContentFetcher().FetchHttpResponseHeader(uri);
				}
			}
			finally {
				if(responseMessage == null) {
					responseMessage.StatusCode = HttpStatusCode.RequestTimeout;
				}
			}

			var page = new Page {
				PageID = Guid.NewGuid() ,
				HttpStatusCode = responseMessage.StatusCode ,
				ResponseHeader = responseMessage.ToString() ,
				PageUri = uri.AbsoluteUri
			};

			page.IsExternal = Crawler.Root.Host != uri.Host;

			page.CrawlState = page.HttpStatusCode != HttpStatusCode.OK || page.IsExternal ? CrawlerState.DontCrawl : CrawlerState.UnCrawled;

			var ucf = new UrlContentFetcher();
			return page;
		}
	}
}
