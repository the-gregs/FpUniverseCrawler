﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;

namespace FpuCrawler.Models {

	public enum CrawlerState {
		UnCrawled , Crawling , Crawled , DontCrawl
	}

	public class Page {
		[Key]
		public Guid PageID { get; set; }
		public string PageUri { get; set; }
		public string ResponseHeader { get; set; }
		public HttpStatusCode HttpStatusCode { get; set; }
		public CrawlerState CrawlState { get; set; }
		public bool IsExternal { get; set; }
	}

	public static class PageHelper {
		public static IEnumerable<Uri> GetLinks(this Page page) {
			var ucf = new UrlContentFetcher();
			return ParseLinks.Parse(ucf.FetchContentbody(new Uri(page.PageUri))).Select(u => u.AbsoluteUri)
				.Distinct().Select(u => u.Contains("http://") || u.Contains("https://") ? new Uri(u) : new Uri(Crawler.Root.AbsoluteUri + u.Substring(1)));
		}
	}
}
