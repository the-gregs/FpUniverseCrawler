namespace FpuCrawler.Models {
	using System;
	using System.Data.Entity;
	using System.Linq;

	public class FpuCrawlerDB : DbContext {
		// Your context has been configured to use a 'FpuCrawlerDB' connection string from your application's 
		// configuration file (App.config or Web.config). By default, this connection string targets the 
		// 'FpuCrawler.Models.FpuCrawlerDB' database on your LocalDb instance. 
		// 
		// If you wish to target a different database and/or database provider, modify the 'FpuCrawlerDB' 
		// connection string in the application configuration file.
		public FpuCrawlerDB()
			: base("name=FpuCrawlerDB") {
		}

		// Add a DbSet for each entity type that you want to include in your model. For more information 
		// on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

		// public virtual DbSet<MyEntity> MyEntities { get; set; }

		public virtual DbSet<Page> Pages { get; set; }

		public static FpuCrawlerDB GetDbContext() {
			return new FpuCrawlerDB();
		}
	}

	//public class MyEntity
	//{
	//    public int Id { get; set; }
	//    public string Name { get; set; }
	//}
}