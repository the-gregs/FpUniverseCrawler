﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace FpuCrawler {
	class UrlContentFetcher {
		public HttpResponseMessage FetchHttpResponseHeader(Uri targetUri) {
			HttpClient client = new HttpClient { MaxResponseContentBufferSize = Int32.MaxValue };
			HttpResponseMessage message = null;
			try {
				message = client.GetAsync(targetUri , HttpCompletionOption.ResponseHeadersRead).Result;
			}
			catch(Exception e) {
				Console.WriteLine("Connection closed unexpectedly: {0}\n{1}" ,targetUri.AbsoluteUri , e.Message);
				message = new HttpResponseMessage();
			}

			if(message == null) {
				message = new HttpResponseMessage();
			}

			return message;
		}

		public string FetchContentbody(Uri targetUri) {
			HttpClient client = new HttpClient { MaxResponseContentBufferSize = Int32.MaxValue };
			return client.GetStringAsync(targetUri).Result;
		}
	}
}
